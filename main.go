package main

/**
 * Нужно создать хттп сервис,
 * в который будут приходить пост запрос с хтмл формы типа регистрация (поля имя и почта),
 * эти данные сохраняются во в памяти приложения и по отдельному хендлеру выдаются списком.
 * Например
 * Post  /set
 * Get /get
 * Учесть вопрос конкурентности, если придут одновременно 2 запроса, чтобы не было паники
 */

/**
 * Я не смог обеспечить выполнение параллельно запросов.
 * Golang не позволяет этого сделать.
 * Всё что мне приходит в голову, это правка исходников server.go
 * Но проблема ещё в том, что в исходниках имеется соответствующий комментарий по этому поводу.
 * https://golang.org/src/net/http/server.go#L1945
 * Детачить же в программе методы Get и Set не имеет смысла,
 * поскольку тогда через интерфейс http.ResponseWriter уже ничего не предать.
 * Использование мютексов в этой программе бесполезно и почти ничего не даёт.
 * Возможно я вообще что-то не то сделал. Я даже условие задачи не мог долго понять.
 */

import (
	"log"
	"net/http"
	"runtime"
)

func main() {
	log.Println(runtime.GOMAXPROCS(-1))
	http.HandleFunc("/", handler)
	http.HandleFunc("/results", resultsHandler)
	http.ListenAndServe(":8080", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		records.GetForm(w)
	case http.MethodPost:
		records.Set(r)
		http.Redirect(w, r, "/", http.StatusFound)
	default:
		log.Println("Cannot handle", r)
	}
}

func resultsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		records.Get(w)
	default:
		log.Println("Cannot handle", r)
	}
}
