package main

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"strings"
	"sync"
)

var pages = template.Must(template.ParseGlob("html/templates/*.html"))
var records storage

type storage struct {
	regRecords []record
	mu         sync.Mutex
}

type record struct {
	name string
	mail string
}

func (s *storage) append(value *record) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.regRecords = append(s.regRecords, *value)
}

func (s *storage) getList() (result []string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	for _, item := range s.regRecords {
		var b strings.Builder
		fmt.Fprintf(&b, "имя: %s почта: %s", item.name, item.mail)
		result = append(result, b.String())
	}
	return
}

func (s *storage) Set(r *http.Request) {
	r.ParseForm()
	var value = record{
		name: r.PostForm.Get("name"),
		mail: r.PostForm.Get("mail")}
	go s.append(&value)
}

func (s *storage) Get(w io.Writer) {
	var recList = s.getList()
	pages.ExecuteTemplate(w, "results.html", recList)
}

func (s *storage) GetForm(w io.Writer) {
	pages.ExecuteTemplate(w, "index.html", nil)
}
